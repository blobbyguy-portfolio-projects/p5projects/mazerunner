class GreedyDepthMaze{
    constructor(rows, cols,startX,startY,endX,endY){
      this.rows = rows;
      this.cols = cols;
      this.Maze = new Maze(rows,cols,startX,startY,endX,endY);
      this.startX = startX
      this.endX = endX

      this.startY = startY;
      this.endY = endY;
      this.success = false;
      this.reset();
    }
  
    reset(){
      this.Maze.reset();
      this.bestPath = new Array();
      this.bestPath.push(this.Maze.grid[this.startX][this.startY]);
      this.finished = false;
      this.success = false;
      
      this.MoveCount = 0;
    }

    draw(sketch){
        if (this.Maze.grid.length > 0){
            console.log("Finished: " + this.finished);
            if (!this.finished){
              this.nextSolutionStep();
            }
        }
        this.Maze.draw(sketch);
        var bottom = this.rows * mazeSquareSizeLength;

        console.log(bottom);
        sketch.fill(0,0,0)
        sketch.textSize(15);
        sketch.text(this.getOutputString(), 0, sketch.height);

        var delta = 255/this.bestPath.length;
        var red = 255;
        var green = 0;
        if (this.finished == true){
          this.bestPath.forEach(pix => {
            if (this.success){
              pix.finishedColour = [red-=delta, green+=delta, 0];
            }else{
              pix.finishedColour = [255,0,0];
            }
          })
        }
    }

    getGreenToRedArray(){
      
    }

    getOutputString(){
      var str = "";
      str += "(Greedy Depth First Maze) "
      str += "Move Count:" + this.MoveCount;
      str += " Finished: " + this.finished;
      str += " Success: " + this.success;

      return str;
    }
  
    nextSolutionStep(){
      if (this.finished == true){
        return;
      }

      var frontOfPath = this.bestPath[this.bestPath.length - 1];

      //Look at adjacent left right up and down
      var options = GetSurroundingOptions(frontOfPath.x, frontOfPath.y, this.Maze.grid);
  
      //Filter out invalid options
      options = options.filter(pix => {
        return !pix.isWall && !pix.partOfPath && !pix.isBadPath;
      })
  
      //Check if there is anywhere to go
      if (options.length == 0){
        //There are no options - no where to go
        frontOfPath.isBadPath = true;
        frontOfPath.partOfPath = false;
  
        var popped = this.bestPath.pop();
  
        if (popped.x == this.startX && popped.y == this.startY){
          //We just popped off the start
          console.log("Back at start with no where to move");
          this.finished = true;
        }

        if (popped == null){
          this.finished = true;
        }
        return;
      }
  
      //Find the closest to the end
      var smallest = Number.MAX_SAFE_INTEGER;
      options.forEach(pix => {
        if (pix.DistanceFromEnd < smallest ){
          frontOfPath = pix;
          smallest = pix.DistanceFromEnd
        }
      })
  
      //Signal to the square it is part of the path less intense drawing function.
      frontOfPath.partOfPath = true;
      if (frontOfPath.x == this.endX && frontOfPath.y == this.endY){
        console.log("Reached the end");
        this.finished = true;
        this.success = true;
      }

      this.MoveCount += 1;
      //Push it onto the path
      this.bestPath.push(frontOfPath);
    }
  }