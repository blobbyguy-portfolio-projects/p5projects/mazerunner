class Pix{
    constructor(x,y, rows, cols, isWall){

      this.x = x;
      this.y = y;
      
      var xDif = (rows - x);
      var yDif = (cols - y);
  
      this.partOfPath = false;
      this.DistanceFromEnd = Math.sqrt(xDif*xDif + yDif*yDif);
      this.isWall = isWall;
      this.isBadPath = false;
      this.isOption = false;
      this.isStart = false;
      this.isEnd = false;

      this.finishedColour = null;
    }
  
    draw(sketch){
        var xCoord = this.x * mazeSquareSizeLength;
        var yCoord = this.y * mazeSquareSizeLength;

      if (this.finishedColour != null){
        sketch.fill(this.finishedColour[0], this.finishedColour[1], this.finishedColour[2]);
      }
      else if (this.isStart){
        sketch.fill(0,255,0)
      }else if (this.isEnd){
        sketch.fill(255,0,0)
      }
      else if (this.partOfPath){
        sketch.fill (0,0,255)
      }else if (this.isWall){
        sketch.fill(0)
      }else if (this.isOption){
        sketch.fill(125,125,125)
      }
      else if (this.isBadPath){
        sketch.fill(125)
      }
      else{
        sketch.fill(255)
      }
  
      sketch.square(xCoord,yCoord,mazeSquareSizeLength - 1)
      
      if (showWeights){
        sketch.fill(50)
        sketch.text(this.DistanceFromEnd, xCoord,yCoord, xCoord+mazeSquareSizeLength, yCoord+mazeSquareSizeLength)
      }
    }
  }