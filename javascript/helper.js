//Returns the perimeter options by reference for a given x and y and a grid
function GetSurroundingOptions(currentX, currentY, grid) {
    var pixes = new Array();
    if (currentX > 0) {
      pixes.push(grid[currentX - 1][currentY]);
    }
    if (currentY > 0) {
      pixes.push(grid[currentX][currentY - 1]);
    }
    if (currentX < cols - 1) {
      pixes.push(grid[currentX + 1][currentY]);
    }
    if (currentY < rows - 1) {
      pixes.push(grid[currentX][currentY + 1]);
    }
  
    if (allowDiagonals) {
      if (currentX > 0 && currentY > 0) {
        pixes.push(grid[currentX - 1][currentY - 1]);
      }
      if (currentY < rows - 1 && currentX < cols - 1) {
        pixes.push(grid[currentX + 1][currentY + 1]);
      }
      if (currentX < cols - 1 && currentY > 0) {
        pixes.push(grid[currentX + 1][currentY - 1]);
      }
      if (currentY < rows - 1 && currentX > 0) {
        pixes.push(grid[currentX - 1][currentY + 1]);
      }
    }
  
    return pixes;
  }