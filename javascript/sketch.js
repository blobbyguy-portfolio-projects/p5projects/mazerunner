
//========== GLOBAL SETTINGS ==========
var showWeights = false;
var allowDiagonals = true;
var mazeSquareSizeLength = 10;
//Width in cubes
var rows = 50; 
var cols = 50;
var wallPercentage = 0.5;

//Calculated global properties
var bottomText = 20;
let depthMaze = function(p){
  
  var greedyDepthMaze;
  var canvasWidth;
  var canvasHeight;

  p.setup = function(){

    //Start at top left, end at bottom right
    startX = 0;
    endX = cols - 1;
    
    startY = 0;
    endY = rows - 1;
  
    //Set up canvas
    canvasWidth = mazeSquareSizeLength * rows;
    canvasHeight = mazeSquareSizeLength * cols;
    p.createCanvas(canvasWidth, canvasHeight + bottomText);
    greedyDepthMaze = new GreedyDepthMaze(rows, cols, startX, startY, endX, endY);
  };
  function resetGreedyDepthMazeMaze(){
    greedyDepthMaze.reset();
  }

  p.draw = function(){

      p.background(200);
      //p.translate(0, topBuffer);
      greedyDepthMaze.draw(this);
  }
}
let myp5 = new p5(depthMaze);





