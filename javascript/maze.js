//Container to hold information regarding cells.
//A lot of these could be calculated on the fly with each frame.
//Prioritizes processing time over memory useage.
class Maze{
    constructor(rows, cols,startX,startY,endX, endY){
      this.rows = rows;
      this.cols = cols;

      this.startX = startX
      this.endX = endX

      this.startY = startY;
      this.endY = endY;
      this.reset();
    }
    reset(){
      this.grid = new Array(cols);
      for (var i = 0; i < cols; i++){
        //Create a 2d array
        this.grid[i] = new Array(rows);
  
        //Fill the row
        for (var j = 0; j < rows; j++){
          var isWall = Math.random() <= wallPercentage;
          this.grid[i][j] = new Pix(i, j, rows, cols, isWall);
        }
      }
      //Ensure the start and ends are not walls
      this.grid[this.startX][this.startY].isWall = false;
      this.grid[this.endX][this.endY].isWall = false;

      this.grid[this.startX][this.startY].isStart = true;
      this.grid[this.endX][this.endY].isEnd = true;
    }

    draw(sketch){
        for (var i = 0; i < rows; i++){
            for (var j = 0; j < cols; j++){
                this.grid[i][j].draw(sketch);
            }
        }
    }
  }