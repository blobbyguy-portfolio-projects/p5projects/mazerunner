class AStarMaze{
    constructor(rows, cols){
      this.Maze = new Maze(rows,cols);
      this.reset();
    }
  
    reset(){
      this.Maze.reset();
      this.bestPath = new Array();
      this.bestPath.push(this.Maze.grid[startX][startY]);
      this.finished = false;
      this.allOptions = new Array();
    }
  
    nextSolutionStep(){
      var frontOfPath = this.bestPath[this.bestPath.length - 1];
  
      //Look at adjacent left right up and down
      var options = GetSurroundingOptions(frontOfPath.x, frontOfPath.y, this.Maze.grid);
  
      //Filter out invalid options
      newOptions = options.filter(pix => {
        return !pix.isWall && !pix.partOfPath && !pix.isBadPath && !pix.isOption;
      })
  
      this.allOptions.push(newOptions);
  
      //Check if there is anywhere to go
      if (allOptions.length == 0){
        console.log("There is no where to move");
        this.finished = true;
      }
  
      //Find the closest to the end out of all options
      var allOptions = Number.MAX_SAFE_INTEGER;
      options.forEach(pix => {
        if (pix.DistanceFromEnd < smallest ){
          frontOfPath = pix;
          smallest = pix.DistanceFromEnd
        }
      })
  
      //Signal to the square it is part of the path less intense drawing function.
      frontOfPath.partOfPath = true;
      if (frontOfPath.x == endX && frontOfPath.y == endY){
        console.log("Reached the end");
        this.finished = true;
      }
      
      //Push it onto the path
      this.bestPath.push(frontOfPath);
    }
  }