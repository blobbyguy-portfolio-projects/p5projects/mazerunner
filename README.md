URL: https://blobbyguy-portfolio-projects.gitlab.io/p5projects/mazerunner

This is just an interest project of mine. 
I want to create a side by side comparison of a different maze algorithms.

While this will compare the number of steps required, it does not compare the processing required for each step.
It is primarily just a visual representation and does not always represent the efficiency of an algorithm.

### Current implementations:
- Greedy Depth First


### TODO: 
- Greedy Breadth
- A*
- Comparison
